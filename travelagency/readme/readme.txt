Two examples are created :
   1) ta_docker_test.yml is used for testing under the docker ,the following parameters need to be confirmed before running:
        a) role chrismeyersfsu.docker-provision is download from the epicon git
        b) you have the iamge 'chrismeyersfsu' in your ansible control node server , it can be changed to your own image.
        c) the ports 3306,9080,9090 are not occupied in your docker container node.
   2) ta_normal_test.yml is used for testing under the normal linux servers , the following parameters need to be confirmed before running:
       a) Go to step "prepare the hosts " , and change them to the server details you are going to run .
       b) Change the "172.17.0.2" to the mysql ip
       c) Change the "172.17.0.3" to the talogic ip
       d) Change the "172.17.0.4" to the taweb ip.
